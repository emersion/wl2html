import os
import sys
from jinja2 import Environment, FileSystemLoader, select_autoescape
from xml.etree import ElementTree

env = Environment(
    loader=FileSystemLoader(os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "templates")),
    autoescape=select_autoescape()
)

def get_summary_and_description(el):
    desc = el.find("description")
    if desc is not None:
        return (desc.attrib["summary"], desc.text)
    else:
        return (el.attrib.get("summary"), None)

class Arg:
    def __init__(self, el):
        self.kind = "arg"
        self.name = el.attrib["name"]
        self.type = el.attrib["type"]
        self.interface = el.attrib.get("interface")
        self.allow_null = bool(el.attrib.get("allow-null", False))
        self.enum = el.attrib.get("enum")
        (self.summary, self.description) = get_summary_and_description(el)

class Message:
    def __init__(self, el):
        self.kind = el.tag
        self.name = el.attrib["name"]
        self.type = el.attrib.get("type")
        self.since = int(el.attrib.get("since", 1))
        (self.summary, self.description) = get_summary_and_description(el)
        self.args = [Arg(child) for child in el.findall("arg")]

class Entry:
    def __init__(self, el):
        self.kind = "entry"
        self.name = el.attrib["name"]
        self.value = el.attrib["value"]
        self.since = int(el.attrib.get("since", 1))
        (self.summary, self.description) = get_summary_and_description(el)

class Enum:
    def __init__(self, el):
        self.kind = "enum"
        self.name = el.attrib["name"]
        self.since = int(el.attrib.get("since", 1))
        self.bitfield = bool(el.attrib.get("bitfield", False))
        (self.summary, self.description) = get_summary_and_description(el)
        self.entries = [Entry(child) for child in el.findall("entry")]

class Interface:
    def __init__(self, el):
        self.kind = "interface"
        self.name = el.attrib["name"]
        self.version = int(el.attrib["version"])
        (self.summary, self.description) = get_summary_and_description(el)
        self.requests = [Message(child) for child in el.findall("request")]
        self.events = [Message(child) for child in el.findall("event")]
        self.enums = [Enum(child) for child in el.findall("enum")]

class Protocol:
    def __init__(self, el):
        self.kind = "protocol"
        self.name = el.attrib["name"]
        self.copyright = getattr(el.find("copyright"), "text", None)
        (self.summary, self.description) = get_summary_and_description(el)
        self.interfaces = [Interface(child) for child in el.findall("interface")]

tree = ElementTree.parse(sys.argv[1])
protocol = Protocol(tree.getroot())

template = env.get_template("protocol.html")
print(template.render(protocol=protocol))
